__Binaries can be downloaded from the [Releases page](https://gitlab.com/gitlab-com/support/toolbox/fast-stats/-/releases)__

A tool with minimal memory use to quickly create and compare performance statistics from and between GitLab logs.
Output can be printed as text, csv, or markdown and sorted by any field.

This is very similar to [json_stats](https://gitlab.com/gitlab-com/support/toolbox/json_stats), but faster and with the ability to compare files.
For comparison, on a 6-core machine `fast-stats` processes a 1.2 GB file in < 1 second vs 6+ minutes for `json_stats`.

#### Support Log Types and Formats
   * production_json
   * api_json
   * gitaly
      * unstructured
      * JSON
   * sidekiq
      * unstructured
      * JSON

#### Field Definitions

   * `COUNT` -- Number of requests
   * `RPS` -- Requests per second
   * `PERC99` -- 99th percentile request duration
   * `PERC95` -- 95th percentile request duration
   * `MEDIAN` -- Median request duration
   * `MAX` -- Longest request duration
   * `MIN` -- Shortest request duration
   * `% FAIL` -- Percentage of requests that failed.  For `api_json` and `prod_json` this is defined as a status >= 500.
   * `SCORE` -- `COUNT` * `PERC99`

#### Examples

Stats for a single file:

```
METHOD                          COUNT     RPS     PERC99     PERC95     MEDIAN           MAX       MIN        SCORE    % FAIL
SSHUploadPack                     127    0.00  324563.03  304192.65  296331.16  553982100.00     37.62  41219504.51      2.36
ListConflictFiles                  19    0.00   59379.50   57959.05      72.03      59734.61      1.97   1128210.49     10.53
ListCommitsByOid                 7706    0.19      11.96       9.71       3.02         18.20      2.00     92192.66      0.00
```

A `--compare` between files using the `--mini` flag for more compact output:

```
FILE     CONTROLLER                                     COUNT     RPS    PERC99    PERC95    MEDIAN       MAX       MIN         SCORE    % FAIL
rails_1  Projects::MergeRequestsController#show         91291    1.06   2766.14   1241.98    383.06  23159.44      5.30  252523686.74      0.00
rails_2                                                 90060    1.04   2662.45   1160.40    383.12  35155.63      5.53  239779922.78      0.13
ratio                                                    0.99x   0.99x     0.96x     0.93x     1.00x     1.52x     1.04x         0.95x      infx

rails_1  Projects::RefsController#logs_tree             3551    0.04  24847.47  11261.35   1694.53  63242.66    118.40   88233348.22      0.17
rails_2                                                 3312    0.04  24334.91  10391.73   1583.68  65727.19    129.10   80597212.65      0.24
ratio                                                   0.93x   0.93x     0.98x     0.92x     0.93x     1.04x     1.09x         0.91x     1.43x

rails_1  Projects::PipelinesController#index            4777    0.06   7525.83   4760.22   1462.51  20880.69     20.94   35950893.73      0.00
rails_2                                                 4025    0.05  14392.68   4254.82   1469.89  17801.20     21.43   57930553.10      0.07
ratio                                                   0.84x   0.84x     1.91x     0.89x     1.01x     0.85x     1.02x         1.61x      infx
```

### Usage

```
fast-stats [FLAGS] [OPTIONS] <FILE>
```

#### Args

##### \<FILE>

Specify file to process. If this is omitted then stdin will be read.

```
$ fast-stats FILE1

METHOD                             COUNT     RPS     PERC99     PERC95     MEDIAN        MAX        MIN        SCORE    % FAIL
InfoRefsUploadPack                 11901    0.80    8282.37    2348.66     147.90  105313.35      27.26  98568497.27      0.00
FindCommits                        11020    0.74    5212.44    3747.11     349.65   40744.88      34.70  57441078.22      0.02
PostUploadPack                       688    0.05   79700.38   29351.74    1037.55  228584.11      91.50  54833861.34      0.58
```

#### Flags

##### --color-output / -C

Force text output to be in color. By default `fast-stats` only outputs colored
text if stdout is a terminal.

##### --mini / -m

Output using a more compact format - only `COUNT`, `PERC99` and `% FAIL` are printed.

This can be used for single file stats and comparisons.

```
$ fast-stats --mini FILE1 --compare FILE2

FILE      METHOD                             COUNT     RPS     PERC99    % FAIL
gitaly_1  FindCommits                        11020    0.74    5212.44      0.02
gitaly_2                                     19919    0.62    7474.36      0.06
ratio                                         1.81x   0.83x      1.43x     3.32x
```

#### Options

###### --bench / -b \<BENCH_VERSION>

Compare performance against GitLab.com. Versions 12.0+ available.

```
$ fast-stats FILE1 --bench 12.1

FILE   CONTROLLER                               COUNT      RPS    PERC99    PERC95    MEDIAN        MAX       MIN         SCORE    % FAIL
12.1   Projects::MergeRequestsController#show     556    11.83   1877.25   1071.27    226.43   10250.30     10.69    1043749.05      0.00
file1                                          120783     1.40   2779.40   1465.08    420.07   25287.60      5.77  335703883.69      0.00
ratio                                          217.24x    0.12x     1.48x     1.37x     1.86x      2.47x     0.54x       321.63x     0.00x
```

###### --compare / -c \<COMPARE_FILE>

Two files of the same type can be compared, with ratio of one to the other listed.

**NOTE**: The logs do NOT need to be the same format, just the same type.  For example, an unstructured Gitaly log can be compared to a JSON gitaly log.

```
$ fast-stats FILE1 --compare FILE2

FILE                  CONTROLLER                COUNT     RPS    PERC99    PERC95    MEDIAN        MAX       MIN         SCORE    % FAIL
file1  Projects::MergeRequestsController#show  120783    1.40   2779.40   1465.08    420.07   25287.60      5.77  335703883.69      0.00
file2                                          118640    1.37   3143.61   1665.49    472.63   39722.20      5.89  372957475.16      0.00
ratio                                            0.98x   0.98x     1.13x     1.14x     1.13x      1.57x     1.02x         1.11x     0.00x
```

##### --format / -f \<FORMAT>

Determine print format, defaults to text.

Available formats:
   * text
   * csv
   * md
   
```
$ fast-stats --format md FILE1
```

|METHOD|COUNT|RPS|PERC99|PERC95|MEDIAN|MAX|MIN|SCORE|% FAIL|
|--|--|--|--|--|--|--|--|--|--|
|InfoRefsUploadPack|11901|0.80|8282.37|2348.66|147.90|105313.35|27.26|98568497.27|0.00%|
|FindCommits|11020|0.74|5212.44|3747.11|349.65|40744.88|34.70|57441078.22|0.02%|

##### -- limit / -l \<LIMIT_CT>

The number of rows / comparisons to print. Defaults to full results.

```
$ fast-stats FILE --limit 2 

METHOD                      COUNT     RPS     PERC99     PERC95     MEDIAN        MAX        MIN        SCORE    % FAIL
FindAllTags                    31    0.02  540180.22  531772.92   12356.73  543566.06     545.93  16745586.94     29.03
ListCommitsByOid               50    0.02  153146.81      14.89       6.08  300269.94       0.05   7657340.49      6.00
```

##### --type / -t \<LOG_TYPE>

`fast-stats` will attempt to automatically determine the type of log files being passed based on their first line.
However, in cases where this is not valid (line partially cut off, stack trace in Sidekiq, etc.) you can override 
the autodetection and manually specify the type of log.

This argument will be applied to both \<FILE> and \<COMPARE_FILE> when using `--compare`

Available types:
   * api_json
   * gitaly
   * gitaly_json
   * production_json
   * sidekiq
   * sidekiq_json

```
$ fast-stats FILE1 --type gitaly

METHOD                        COUNT        PERC99        PERC95      MEDIAN           MAX         MIN           SCORE      % FAIL
FindMergeBase                  9073      23911.54       3953.48      622.12      35528.61       49.23    216949361.77        0.00
```

##### --sort-by / -s \<SORT_BY>

Which field to sort results by, defaults to SCORE. Sort is always descending.

Available fields to sort by:
   * count
   * fail
   * max
   * median
   * min
   * perc95
   * perc99
   * rps
   * score

```
$ fast-stats --sort-by fail FILE1

METHOD                             COUNT     RPS     PERC99     PERC95     MEDIAN        MAX        MIN        SCORE    % FAIL
UserCreateBranch                      38    0.00  110000.97  110000.65    3307.07  110001.02     620.83   4180036.93     13.16
CalculateChecksum                    536    0.04    8916.93    2427.68     124.20   20013.06      27.92   4779475.12     12.50
UserCommitFiles                       47    0.00  110000.45  100471.56    3650.78  110000.68     630.34   5170021.15      8.51
```
