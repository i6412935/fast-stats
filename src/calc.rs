use crate::event::{Event, Status};
use crate::raw_data::RawData;
use crate::stats_vec::{SortBy, StatsVec};
use crate::HashMap;
use chrono::Duration;
use crossbeam_channel::bounded as bounded_channel;
use crossbeam_channel::{Receiver, Sender};
use std::io::{prelude::*, Lines};
use std::thread;

pub fn calc_stats_st<T, B>(lines: Lines<B>, sort_by: SortBy, first_line: Option<String>) -> StatsVec
where
    T: Event,
    B: BufRead,
{
    let mut data_map = HashMap::default();
    let mut iter = lines.peekable();
    let mut start_time = None;

    match first_line {
        Some(fl) => {
            start_time = T::parse_time(&fl);
            parse_line::<T>(&fl, &mut data_map);
        }
        None => {
            if let Some(Ok(fl)) = iter.next() {
                start_time = T::parse_time(&fl);
                parse_line::<T>(&fl, &mut data_map);
            }
        }
    };

    let mut end_time = None;
    while let Some(Ok(line)) = iter.next() {
        if iter.peek().is_none() {
            end_time = T::parse_time(&line);
        }

        parse_line::<T>(&line, &mut data_map);
    }

    let dur = match (start_time, end_time) {
        (Some(s), Some(e)) => e - s,
        _ => Duration::zero(),
    };

    calculate(data_map, dur, sort_by)
}

pub fn calc_stats_mt<T, B>(
    lines: Lines<B>,
    sort_by: SortBy,
    thread_ct: usize,
    first_line: Option<String>,
) -> StatsVec
where
    T: Event,
    B: BufRead,
{
    let (tx, rx): (Sender<String>, Receiver<_>) = bounded_channel(64);

    let thread_handles: Vec<_> = (0..thread_ct - 1)
        .map(|_| {
            let local_rx = rx.clone();
            thread::spawn(move || receive_lines::<T>(local_rx))
        })
        .collect();

    let mut iter = lines.peekable();
    let mut start_time = None;

    match first_line {
        Some(fl) => {
            start_time = T::parse_time(&fl);
            tx.send(fl).expect("Failed to send first line");
        }
        None => {
            if let Some(Ok(fl)) = iter.next() {
                start_time = T::parse_time(&fl);
                tx.send(fl).expect("Failed to send first line");
            }
        }
    };

    let mut end_time = None;
    while let Some(Ok(line)) = iter.next() {
        if iter.peek().is_none() {
            end_time = T::parse_time(&line);
        }

        tx.send(line).expect("Failed to send to input channel");
    }

    let dur = match (start_time, end_time) {
        (Some(s), Some(e)) => e - s,
        _ => Duration::zero(),
    };

    drop(tx);

    let data_map = thread_handles
        .into_iter()
        .filter_map(|h| h.join().ok())
        .fold(HashMap::default(), |map, thd_map| {
            coalesce_maps(map, thd_map)
        });

    calculate(data_map, dur, sort_by)
}

fn receive_lines<T>(rx_input: Receiver<String>) -> HashMap<String, RawData>
where
    T: Event,
{
    let mut local_map = HashMap::default();

    rx_input
        .iter()
        .for_each(|s| parse_line::<T>(&s, &mut local_map));

    local_map
}

fn parse_line<T>(line: &str, data: &mut HashMap<String, RawData>)
where
    T: Event,
{
    if let Some(event) = T::parse(line) {
        let (action, dur, status, stats_type) = event.deconstruct();
        let entry = data
            .entry(action)
            .or_insert_with(|| RawData::new(stats_type));
        entry.data.push(dur);

        if let Status::Fail = status {
            entry.fails += 1;
        }
    }
}

fn coalesce_maps(
    mut main_map: HashMap<String, RawData>,
    local_map: HashMap<String, RawData>,
) -> HashMap<String, RawData> {
    for (action, local_data) in local_map {
        let main_entry = main_map
            .entry(action)
            .or_insert_with(|| RawData::new(local_data.stats_type));
        main_entry.data.extend(local_data.data);
        main_entry.fails += local_data.fails;
    }

    main_map
}

fn calculate(data_map: HashMap<String, RawData>, dur: Duration, sort_by: SortBy) -> StatsVec {
    let mut stats = StatsVec::from((data_map, dur));
    stats.sort_by(sort_by);

    stats
}
