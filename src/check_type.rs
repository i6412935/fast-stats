use crate::{event::*, AppType, LogFormat, LogType};
use serde::Deserialize;
use std::io::{prelude::*, Error, ErrorKind};

pub fn check_log_type(mut reader: impl BufRead) -> Result<(LogType, Option<String>), Error> {
    let mut buff = String::with_capacity(1024);

    while reader.read_line(&mut buff)? > 0 {
        if let Some(log_type) = check_line(&buff) {
            return Ok((log_type, Some(buff)));
        }
        buff.clear();
    }
    Err(Error::new(
        ErrorKind::InvalidInput,
        "Unrecognized log format",
    ))
}

fn check_line(line: &str) -> Option<LogType> {
    use AppType::*;
    use LogFormat::*;

    if ApiJsonEvent::parse(&line).is_some() {
        Some(LogType::new(ApiJson, RailsApi))
    } else if ApiJsonDurEvent::parse(&line).is_some() {
        Some(LogType::new(ApiJsonDurS, RailsApi))
    } else if CheckGitalyJsonEvent::parse(&line).is_some() {
        Some(LogType::new(GitalyJson, Gitaly))
    } else if ProdJsonEvent::parse(&line).is_some() {
        Some(LogType::new(ProdJson, RailsProd))
    } else if ProdJsonDurEvent::parse(&line).is_some() {
        Some(LogType::new(ProdJsonDurS, RailsProd))
    } else if SidekiqJsonEvent::parse(&line).is_some() {
        Some(LogType::new(SidekiqJson, Sidekiq))
    } else if SidekiqJsonDurSEvent::parse(&line).is_some() {
        Some(LogType::new(SidekiqJsonDurS, Sidekiq))
    } else if line.contains("time=") {
        Some(LogType::new(GitalyText, Gitaly))
    } else if line.contains("TID-") {
        Some(LogType::new(SidekiqText, Sidekiq))
    } else {
        None
    }
}

// Valid Gitaly events may not contain the fields we care about
// This struct contains fields unique to Gitaly that are always present
#[derive(Deserialize)]
#[allow(dead_code)]
struct CheckGitalyJsonEvent {
    time: String,
    level: String,
    msg: String,
}

impl CheckGitalyJsonEvent {
    fn parse(s: &str) -> Option<CheckGitalyJsonEvent> {
        serde_json::from_str::<CheckGitalyJsonEvent>(&s).ok()
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use AppType::*;
    use LogFormat::*;

    #[test]
    fn find_api_json() {
        let json = r##"{"time":"2019-04-10T04:47:01.360Z","severity":"INFO","duration":3.82,"db":1.22,"view":2.5999999999999996,"status":200,"method":"GET","path":"/api/v4/internal/authorized_keys","params":[{"key":"key","value":"[FILTERED]"},{"key":"secret_token","value":"[FILTERED]"}],"host":"127.0.0.1","ip":"127.0.0.1","ua":"Ruby","route":"/api/:version/internal/authorized_keys","gitaly_calls":0,"correlation_id":"8d5c2a7d-66f1-4e0d-917b-ae2a65954bf9"}"##;
        assert_eq!(check_line(json), Some(LogType::new(ApiJson, RailsApi)));
    }

    #[test]
    fn find_api_json_dur_s() {
        let json = r##"{"time":"2020-04-23T22:14:20.290Z","severity":"INFO","duration_s":1.46,"db_duration_s":0.09,"view_duration_s":1.37,"status":200,"method":"GET","path":"/api/v4/projects","params":[{"key":"page","value":"2"},{"key":"per_page","value":"100"},{"key":"statistics","value":"true"}],"host":"gitlab.example.com","remote_ip":"10.10.10.10, 10.10.10.10","ua":"go-gitlab","route":"/api/:version/projects","user_id":1,"username":"root","queue_duration_s":0.01,"gitaly_calls":11,"gitaly_duration_s":0.13,"redis_calls":701,"redis_duration_s":0.06,"correlation_id":"u4G996jjXJ1"}"##;
        assert_eq!(check_line(json), Some(LogType::new(ApiJsonDurS, RailsApi)));
    }

    #[test]
    fn find_prod_json() {
        let json = r##"{"method":"GET","path":"/group/project.git/info/refs","format":"html","controller":"Projects::GitHttpController","action":"info_refs","status":200,"duration":147.69,"view":0.34,"db":14.5,"time":"2019-04-19T08:10:14.269Z","params":[{"key":"service","value":"git-upload-pack"},{"key":"namespace_id","value":"group"},{"key":"project_id","value":"project.git"}],"remote_ip":"10.10.10.10","user_id":1,"username":"root","ua":"JGit/3.5.3.201412180710-r","queue_duration":null,"correlation_id":"063a9dd3-c4ee-4dbc-a912-948f9b6da880"}"##;
        assert_eq!(check_line(json), Some(LogType::new(ProdJson, RailsProd)));
    }

    #[test]
    fn find_prod_json_dur_s() {
        let json = r##"{"method":"GET","path":"/group/project/blob/0000000000000000000000000000000000000001/src/__pycache__/__init__.cpython-35.pyc","format":"*/*","controller":"Projects::BlobController","action":"show","status":200,"time":"2020-04-24T04:54:41.461Z","params":[{"key":"namespace_id","value":"group"},{"key":"project_id","value":"project"},{"key":"id","value":"0000000000000000000000000000000000000001/io/__pycache__/__init__.cpython-35.pyc"}],"remote_ip":"10.10.10.10","user_id":null,"username":null,"ua":"Mozilla/5.0 (compatible; )","queue_duration_s":0.01,"gitaly_calls":2,"gitaly_duration_s":0.02,"rugged_calls":5,"rugged_duration_s":0.01,"redis_calls":26,"redis_duration_s":0.0,"correlation_id":"IratXOPyxi6","cpu_s":0.12,"db_duration_s":0.01,"view_duration_s":0.08,"duration_s":0.14}"##;
        assert_eq!(
            check_line(json),
            Some(LogType::new(ProdJsonDurS, RailsProd))
        );
    }

    #[test]
    fn find_gitaly_text() {
        let text = r##"2019-02-01_11:30:20.13239 time="2019-02-01T11:30:20Z" level=info msg="finished streaming call with code OK" grpc.code=OK grpc.meta.auth_version=v2 grpc.method=InfoRefsUploadPack grpc.request.fullMethod=/gitaly.SmartHTTPService/InfoRefsUploadPack grpc.request.glRepository=project-1 grpc.request.repoPath=group/project.git grpc.request.repoStorage=default grpc.request.topLevelGroup=group grpc.service=gitaly.SmartHTTPService grpc.start_time="2019-02-01T11:30:19Z" grpc.time_ms=322.1 peer.address=@ span.kind=server system=grpc"##;
        assert_eq!(check_line(text), Some(LogType::new(GitalyText, Gitaly)));
    }

    #[test]
    fn find_gitaly_json() {
        let json = r##"{"correlation_id":"KikVy1oq2R1","grpc.code":"OK","grpc.meta.auth_version":"v2","grpc.meta.client_name":"gitlab-sidekiq","grpc.method":"FindCommit","grpc.request.deadline":"2019-05-10T09:19:49Z","grpc.request.fullMethod":"/gitaly.CommitService/FindCommit","grpc.request.glProjectPath":"group/project","grpc.request.glRepository":"project-1","grpc.request.repoPath":"@hashed/aa/bb/0000000000000000000000000000000000000000000000000000000000000001.git","grpc.request.repoStorage":"default","grpc.request.topLevelGroup":"@hashed","grpc.service":"gitaly.CommitService","grpc.start_time":"2019-05-10T08:57:18Z","grpc.time_ms":3.373,"level":"info","msg":"finished unary call with code OK","peer.address":"10.10.10.10:50000","span.kind":"server","system":"grpc","time":"2019-05-10T08:57:18Z"}"##;
        assert_eq!(check_line(json), Some(LogType::new(GitalyJson, Gitaly)));
    }

    #[test]
    fn find_sidekiq_text() {
        let text = r##"2019-03-17_02:53:25.22981 2019-03-17T02:53:25.229Z 129639 TID-otfgm18w3 Gitlab::GithubImport::ImportPullRequestWorker JID-c73ed50847d6b5746e45d913 INFO: done: 289.24 sec"##;
        assert_eq!(check_line(text), Some(LogType::new(SidekiqText, Sidekiq)));
    }

    #[test]
    fn find_sidekiq_text_no_dur() {
        let text = r##"2019-03-17_02:52:57.56595 2019-03-17T02:52:57.565Z 129639 TID-otfw8x097 PipelineUpdateWorker JID-86d85e6a53849b3413e21c0c INFO: start"##;
        assert_eq!(check_line(text), Some(LogType::new(SidekiqText, Sidekiq)));
    }

    #[test]
    fn find_sidekiq_json() {
        let json = r##"{"severity":"INFO","time":"2020-04-23T05:19:04.613Z","queue":"cronjob:elastic_index_bulk_cron","class":"ElasticIndexBulkCronWorker","retry":false,"queue_namespace":"cronjob","jid":"1fabed514650adbc9f088be2","created_at":"2020-04-23T05:19:04.557Z","correlation_id":"8ce2d6b874090d8994419d3a921ed457","enqueued_at":"2020-04-23T05:19:04.557Z","pid":29039,"message":"ElasticIndexBulkCronWorker JID-1fabed514650adbc9f088be2: done: 0.052651 sec","job_status":"done","scheduling_latency_s":0.003145,"duration":0.052651,"cpu_s":0.003783,"completed_at":"2020-04-23T05:19:04.613Z","db_duration":0,"db_duration_s":0}"##;
        assert_eq!(check_line(json), Some(LogType::new(SidekiqJson, Sidekiq)));
    }

    #[test]
    fn no_match_sidekiq_json_no_dur() {
        let json = r##"{"severity":"INFO","time":"2020-04-23T05:19:04.560Z","queue":"cronjob:elastic_index_bulk_cron","class":"ElasticIndexBulkCronWorker","retry":false,"queue_namespace":"cronjob","jid":"1fabed514650adbc9f088be2","created_at":"2020-04-23T05:19:04.557Z","correlation_id":"8ce2d6b874090d8994419d3a921ed457","enqueued_at":"2020-04-23T05:19:04.557Z","pid":29039,"message":"ElasticIndexBulkCronWorker JID-1fabed514650adbc9f088be2: start","job_status":"start","scheduling_latency_s":0.003145}"##;
        assert_eq!(check_line(json), None);
    }

    #[test]
    fn find_sidekiq_json_dur_s() {
        let json = r##"{"severity":"INFO","time":"2020-05-08T21:39:40.012Z","class":"BuildFinishedWorker","args":["1"],"retry":3,"queue":"pipeline_processing:build_finished","queue_namespace":"pipeline_processing","jid":"2e2703334a8a5cbbb4463207","created_at":"2020-05-08T21:39:39.938Z","correlation_id":"aIt1a0yfTb2","enqueued_at":"2020-05-08T21:39:39.938Z","pid":559,"message":"BuildFinishedWorker JID-2e2703334a8a5cbbb4463207: done: 0.073214 sec","job_status":"done","scheduling_latency_s":0.000773,"redis_calls":6,"redis_duration_s":0.000809,"duration_s":0.073214,"cpu_s":0.034583,"completed_at":"2020-05-08T21:39:40.012Z","db_duration_s":0.030742}"##;
        assert_eq!(
            check_line(json),
            Some(LogType::new(SidekiqJsonDurS, Sidekiq))
        );
    }

    #[test]
    fn no_match_sidekiq_json_dur_s_no_dur() {
        let json = r##"{"severity":"INFO","time":"2020-05-08T21:39:39.604Z","class":"BuildQueueWorker","args":["1"],"retry":3,"queue":"pipeline_processing:build_queue","queue_namespace":"pipeline_processing","jid":"1de523f594c9ee7417a1675c","created_at":"2020-05-08T21:39:39.602Z","correlation_id":"QnCYtQTJ5O1","enqueued_at":"2020-05-08T21:39:39.603Z","pid":557,"message":"BuildQueueWorker JID-1de523f594c9ee7417a1675c: start","job_status":"start","scheduling_latency_s":0.000836}"##;
        assert_eq!(check_line(json), None);
    }

    #[test]
    fn no_match_gibberish() {
        let text = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.";
        assert_eq!(check_line(text), None);
    }
}
