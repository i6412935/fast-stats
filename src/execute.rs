use crate::calc::{calc_stats_mt, calc_stats_st};
use crate::event::*;
use crate::stats::StatsType;
use crate::stats_vec::{PrintFormat, SortBy, StatsDiffVec, StatsVec};
use crate::{check_type, Args, Input, LogFormat, LogType};
use std::fs::File;
use std::io;
use std::io::{prelude::*, stdin, BufReader, ErrorKind};
use std::thread;

enum Reader {
    File(BufReader<File>),
    Stdin,
}

struct FileParams {
    log_type: LogType,
    reader: Reader,
    first_line: Option<String>,
}

impl FileParams {
    fn deconstruct(self) -> (LogType, Reader, Option<String>) {
        (self.log_type, self.reader, self.first_line)
    }
}

fn open_reader(file_name: &str) -> Result<BufReader<File>, io::Error> {
    let f = File::open(file_name)?;
    Ok(BufReader::new(f))
}

pub fn exec_stats(args: Args) -> Result<(), io::Error> {
    let stats = match args.input {
        Input::File(file_name) => {
            let mut reader = open_reader(file_name)?;

            let (log_type, first_line) = match args.requested_log_type {
                Some(req_type) => (req_type, None),
                None => check_type::check_log_type(&mut reader)?,
            };

            calc(log_type, reader, args.sort_by, args.thread_ct, first_line)
        }
        Input::Stdin => {
            let (log_type, first_line) = match args.requested_log_type {
                Some(req_type) => (req_type, None),
                None => check_type::check_log_type(stdin().lock())?,
            };

            calc(
                log_type,
                stdin().lock(),
                args.sort_by,
                args.thread_ct,
                first_line,
            )
        }
    };
    print_stats(&stats, args.print_format, args.limit)?;

    Ok(())
}

pub fn exec_compare(args: Args, cmp_input: Input) -> Result<(), io::Error> {
    let params = input_params(&args.input, args.requested_log_type)?;
    let cmp_params = input_params(&cmp_input, args.requested_log_type)?;

    if params.log_type.app == cmp_params.log_type.app {
        let sort_by = args.sort_by;
        let thread_ct = args.thread_ct;

        let (log_type, reader, first_line) = params.deconstruct();
        let (cmp_type, cmp_reader, cmp_line) = cmp_params.deconstruct();

        let handle = match reader {
            Reader::File(rd) => {
                thread::spawn(move || calc(log_type, rd, sort_by, thread_ct / 2, first_line))
            }
            Reader::Stdin => thread::spawn(move || {
                calc(log_type, stdin().lock(), sort_by, thread_ct / 2, first_line)
            }),
        };

        let cmp_handle = match cmp_reader {
            Reader::File(rd) => {
                thread::spawn(move || calc(cmp_type, rd, sort_by, thread_ct / 2, cmp_line))
            }
            Reader::Stdin => thread::spawn(move || {
                calc(cmp_type, stdin().lock(), sort_by, thread_ct / 2, cmp_line)
            }),
        };

        let stats = handle.join().expect("Primary thread panicked");
        let cmp_stats = cmp_handle.join().expect("Compare thread panicked");

        let file_name = args.input.file_name();
        let cmp_file_name = cmp_input.file_name();

        let diffs = StatsDiffVec::new(&stats, &cmp_stats, &file_name, &cmp_file_name);

        print_diff_stats(&diffs, args.print_format, args.limit, args.force_color)?;
    } else {
        eprintln!(
            "Log types found -- {} : {}",
            params.log_type.app, cmp_params.log_type.app
        );
        return Err(io::Error::new(
            ErrorKind::InvalidInput,
            "Attempted to compare different log types",
        ));
    }

    Ok(())
}

pub fn exec_bench(args: Args, bench_vers: String) -> Result<(), io::Error> {
    let params = input_params(&args.input, args.requested_log_type)?;
    let thread_ct = args.thread_ct;
    let sort_by = args.sort_by;
    let (log_type, reader, first_line) = params.deconstruct();

    let handle = match reader {
        Reader::File(rd) => {
            thread::spawn(move || calc(log_type, rd, sort_by, thread_ct, first_line))
        }
        Reader::Stdin => {
            thread::spawn(move || calc(log_type, stdin().lock(), sort_by, thread_ct, first_line))
        }
    };
    let file_name = args.input.file_name();
    let stats = handle.join().expect("Primary thread panicked");
    let stats_type = match stats.stats_type() {
        Some(t) => t,
        None => return Ok(()),
    };

    use StatsType::*;
    let bench_stats = include!(concat!(env!("OUT_DIR"), "/bench_fn.rs"));
    let diffs = StatsDiffVec::new(&bench_stats, &stats, &bench_vers, &file_name);

    print_diff_stats(&diffs, args.print_format, args.limit, args.force_color)?;

    Ok(())
}

fn input_params(input: &Input, req_type: Option<LogType>) -> Result<FileParams, io::Error> {
    let params = match (input, req_type) {
        (Input::File(file_name), Some(log_type)) => {
            let reader = open_reader(file_name)?;

            FileParams {
                log_type,
                reader: Reader::File(reader),
                first_line: None,
            }
        }
        (Input::File(file_name), None) => {
            let mut reader = open_reader(file_name)?;

            let (log_type, first_line) = check_type::check_log_type(&mut reader)?;
            FileParams {
                log_type,
                reader: Reader::File(reader),
                first_line,
            }
        }
        (Input::Stdin, Some(log_type)) => FileParams {
            log_type,
            reader: Reader::Stdin,
            first_line: None,
        },
        (Input::Stdin, None) => {
            let (log_type, first_line) = check_type::check_log_type(stdin().lock())?;
            FileParams {
                log_type,
                reader: Reader::Stdin,
                first_line,
            }
        }
    };

    Ok(params)
}

fn calc(
    log_type: LogType,
    reader: impl BufRead,
    sort_by: SortBy,
    thread_ct: usize,
    first_line: Option<String>,
) -> StatsVec {
    match thread_ct {
        0..=1 => match log_type.format {
            LogFormat::ApiJson => {
                calc_stats_st::<ApiJsonEvent, _>(reader.lines(), sort_by, first_line)
            }
            LogFormat::ApiJsonDurS => {
                calc_stats_st::<ApiJsonDurEvent, _>(reader.lines(), sort_by, first_line)
            }
            LogFormat::GitalyText => {
                calc_stats_st::<GitalyEvent, _>(reader.lines(), sort_by, first_line)
            }
            LogFormat::GitalyJson => {
                calc_stats_st::<GitalyJsonEvent, _>(reader.lines(), sort_by, first_line)
            }
            LogFormat::ProdJson => {
                calc_stats_st::<ProdJsonEvent, _>(reader.lines(), sort_by, first_line)
            }
            LogFormat::ProdJsonDurS => {
                calc_stats_st::<ProdJsonDurEvent, _>(reader.lines(), sort_by, first_line)
            }
            LogFormat::SidekiqText => {
                calc_stats_st::<SidekiqEvent, _>(reader.lines(), sort_by, first_line)
            }
            LogFormat::SidekiqJson => {
                calc_stats_st::<SidekiqJsonEvent, _>(reader.lines(), sort_by, first_line)
            }
            LogFormat::SidekiqJsonDurS => {
                calc_stats_st::<SidekiqJsonDurSEvent, _>(reader.lines(), sort_by, first_line)
            }
        },
        _ => match log_type.format {
            LogFormat::ApiJson => {
                calc_stats_mt::<ApiJsonEvent, _>(reader.lines(), sort_by, thread_ct, first_line)
            }
            LogFormat::ApiJsonDurS => {
                calc_stats_mt::<ApiJsonDurEvent, _>(reader.lines(), sort_by, thread_ct, first_line)
            }
            LogFormat::GitalyText => {
                calc_stats_mt::<GitalyEvent, _>(reader.lines(), sort_by, thread_ct, first_line)
            }
            LogFormat::GitalyJson => {
                calc_stats_mt::<GitalyJsonEvent, _>(reader.lines(), sort_by, thread_ct, first_line)
            }
            LogFormat::ProdJson => {
                calc_stats_mt::<ProdJsonEvent, _>(reader.lines(), sort_by, thread_ct, first_line)
            }
            LogFormat::ProdJsonDurS => {
                calc_stats_mt::<ProdJsonDurEvent, _>(reader.lines(), sort_by, thread_ct, first_line)
            }
            LogFormat::SidekiqText => {
                calc_stats_mt::<SidekiqEvent, _>(reader.lines(), sort_by, thread_ct, first_line)
            }
            LogFormat::SidekiqJson => {
                calc_stats_mt::<SidekiqJsonEvent, _>(reader.lines(), sort_by, thread_ct, first_line)
            }
            LogFormat::SidekiqJsonDurS => calc_stats_mt::<SidekiqJsonDurSEvent, _>(
                reader.lines(),
                sort_by,
                thread_ct,
                first_line,
            ),
        },
    }
}

fn print_stats(
    stats: &StatsVec,
    format: PrintFormat,
    limit: Option<usize>,
) -> Result<(), io::Error> {
    match format {
        PrintFormat::FullCsv => stats.print_csv(limit)?,
        PrintFormat::FullMd => stats.print_md(limit)?,
        PrintFormat::FullText => stats.print_text(limit)?,
        PrintFormat::MiniCsv => stats.print_mini_csv(limit)?,
        PrintFormat::MiniMd => stats.print_mini_md(limit)?,
        PrintFormat::MiniText => stats.print_mini_text(limit)?,
    }

    Ok(())
}

fn print_diff_stats(
    stats_diff: &StatsDiffVec,
    format: PrintFormat,
    limit: Option<usize>,
    force_color: bool,
) -> Result<(), io::Error> {
    match format {
        PrintFormat::FullCsv => stats_diff.print_csv(limit)?,
        PrintFormat::FullMd => stats_diff.print_md(limit)?,
        PrintFormat::FullText => stats_diff.print_text(limit, force_color)?,
        PrintFormat::MiniCsv => stats_diff.print_mini_csv(limit)?,
        PrintFormat::MiniMd => stats_diff.print_mini_md(limit)?,
        PrintFormat::MiniText => stats_diff.print_mini_text(limit, force_color)?,
    }

    Ok(())
}
