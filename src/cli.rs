use clap::{App, Arg};

pub fn cli_args() -> App<'static, 'static> {
    App::new("fast stats")
        .version(clap::crate_version!())
        .author(clap::crate_authors!())
        .about("Basic performance statistics for GitLab logs")
        .arg(
            Arg::with_name("file")
                .help("File to be parsed")
                .takes_value(true)
                .value_name("FILE_NAME")
                .number_of_values(1),
        )
        .arg(
            Arg::with_name("compare")
                .help("Calculate performance changes between two files")
                .short("c")
                .long("compare")
                .takes_value(true)
                .value_name("COMPARE_FILE")
                .number_of_values(1),
        )
        .arg(
            Arg::with_name("benchmark")
                .help("Compare performance to GitLab.com")
                .short("b")
                .long("bench")
                .takes_value(true)
                .value_name("BENCH_VERSION")
                .number_of_values(1)
                .possible_values(&["12.0", "12.1", "12.2", "12.3", "12.4", "12.5", "12.6", "12.7", "12.8", "12.9", "12.10"]),
        )
        .arg(
            Arg::with_name("force_color")
                .help("Output colored text even if writing to a pipe or file")
                .short("C")
                .long("color-output"),
        )
        .arg(
            Arg::with_name("format")
                .help("Format to print in (default text)")
                .short("f")
                .long("format")
                .takes_value(true)
                .value_name("FORMAT")
                .possible_values(&["text", "md", "csv"]),
        )
        .arg(
            Arg::with_name("limit")
                .help("Number of results to display")
                .short("l")
                .long("limit")
                .takes_value(true)
                .value_name("LIMIT_CT")
                .number_of_values(1)
                .validator(validate_limit),
        )
        .arg(
            Arg::with_name("log_type")
                .help("Manually specify log type of <INPUT> if log type cannot be deduced automatically")
                .short("t")
                .long("type")
                .takes_value(true)
                .value_name("LOG_TYPE")
                .possible_values(&[
                    "api_json",
                    "api_json_dur_s",
                    "gitaly",
                    "gitaly_json",
                    "production_json",
                    "production_json_dur_s",
                    "sidekiq",
                    "sidekiq_json",
                    "sidekiq_json_dur_s",
                ]),
        )
        .arg(
            Arg::with_name("mini")
                .help("Display only COUNT, P99, and SCORE columns")
                .short("m")
                .long("mini"),
        )
        .arg(
            Arg::with_name("sort_by")
                .help("Field to sort by descending value (default score)")
                .short("s")
                .long("sort-by")
                .takes_value(true)
                .value_name("SORT_BY")
                .possible_values(&[
                    "score", "count", "rps", "max", "median", "min", "perc100", "perc95", "fail",
                ]),
        )
}

fn validate_limit(c: String) -> Result<(), String> {
    if c.parse::<usize>().is_ok() {
        return Ok(());
    }
    Err(String::from("COUNT must be a non-negative integer"))
}
