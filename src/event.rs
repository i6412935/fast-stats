use crate::stats::StatsType;
use chrono::prelude::*;
use serde::Deserialize;
use serde_json::Value;

pub trait Event: Sized {
    fn deconstruct(self) -> (String, f64, Status, StatsType);
    fn parse(s: &str) -> Option<Self>;
    fn parse_time(s: &str) -> Option<NaiveDateTime>;
}

#[derive(Clone, Copy, Debug)]
pub enum Status {
    Success,
    Fail,
}

#[derive(Deserialize)]
pub struct ApiJsonEvent {
    route: String,
    status: f64,
    duration: f64,
}

impl Event for ApiJsonEvent {
    #[inline]
    fn deconstruct(self) -> (String, f64, Status, StatsType) {
        let status = if self.status >= 500.0 {
            Status::Fail
        } else {
            Status::Success
        };
        (self.route, self.duration, status, StatsType::Api)
    }

    #[inline]
    fn parse(s: &str) -> Option<ApiJsonEvent> {
        serde_json::from_str::<ApiJsonEvent>(&s).ok()
    }

    fn parse_time(s: &str) -> Option<NaiveDateTime> {
        let untyped: Value = serde_json::from_str(s).ok()?;
        let time_str = untyped.get("time").and_then(|s| s.as_str())?;
        NaiveDateTime::parse_from_str(&time_str[..time_str.len() - 1], "%Y-%m-%dT%H:%M:%S%.f").ok()
    }
}

#[derive(Deserialize)]
pub struct ApiJsonDurEvent {
    route: String,
    status: f64,
    duration_s: f64,
}

impl Event for ApiJsonDurEvent {
    #[inline]
    fn deconstruct(self) -> (String, f64, Status, StatsType) {
        let status = if self.status >= 500.0 {
            Status::Fail
        } else {
            Status::Success
        };
        (self.route, self.duration_s * 1000.0, status, StatsType::Api)
    }

    #[inline]
    fn parse(s: &str) -> Option<ApiJsonDurEvent> {
        serde_json::from_str::<ApiJsonDurEvent>(&s).ok()
    }

    fn parse_time(s: &str) -> Option<NaiveDateTime> {
        let untyped: Value = serde_json::from_str(s).ok()?;
        let time_str = untyped.get("time").and_then(|s| s.as_str())?;
        NaiveDateTime::parse_from_str(&time_str[..time_str.len() - 1], "%Y-%m-%dT%H:%M:%S%.f").ok()
    }
}

#[derive(Deserialize)]
pub struct ProdJsonEvent {
    controller: String,
    action: String,
    status: f64,
    duration: f64,
}

impl Event for ProdJsonEvent {
    #[inline]
    fn deconstruct(self) -> (String, f64, Status, StatsType) {
        let status = if self.status >= 500.0 {
            Status::Fail
        } else {
            Status::Success
        };
        (
            format!("{}#{}", self.controller, self.action),
            self.duration,
            status,
            StatsType::Prod,
        )
    }

    #[inline]
    fn parse(s: &str) -> Option<ProdJsonEvent> {
        serde_json::from_str::<ProdJsonEvent>(&s).ok()
    }

    fn parse_time(s: &str) -> Option<NaiveDateTime> {
        let untyped: Value = serde_json::from_str(s).ok()?;
        let time_str = untyped.get("time").and_then(|s| s.as_str())?;
        NaiveDateTime::parse_from_str(&time_str[..time_str.len() - 1], "%Y-%m-%dT%H:%M:%S%.f").ok()
    }
}

#[derive(Deserialize)]
pub struct ProdJsonDurEvent {
    controller: String,
    action: String,
    status: f64,
    duration_s: f64,
}

impl Event for ProdJsonDurEvent {
    #[inline]
    fn deconstruct(self) -> (String, f64, Status, StatsType) {
        let status = if self.status >= 500.0 {
            Status::Fail
        } else {
            Status::Success
        };
        (
            format!("{}#{}", self.controller, self.action),
            self.duration_s * 1000.0,
            status,
            StatsType::Prod,
        )
    }

    #[inline]
    fn parse(s: &str) -> Option<ProdJsonDurEvent> {
        serde_json::from_str::<ProdJsonDurEvent>(&s).ok()
    }

    fn parse_time(s: &str) -> Option<NaiveDateTime> {
        let untyped: Value = serde_json::from_str(s).ok()?;
        let time_str = untyped.get("time").and_then(|s| s.as_str())?;
        NaiveDateTime::parse_from_str(&time_str[..time_str.len() - 1], "%Y-%m-%dT%H:%M:%S%.f").ok()
    }
}

pub struct GitalyEvent {
    method: String,
    time_ms: f64,
    status: Status,
}

impl Event for GitalyEvent {
    #[inline]
    fn deconstruct(self) -> (String, f64, Status, StatsType) {
        (self.method, self.time_ms, self.status, StatsType::Gitaly)
    }

    #[inline]
    fn parse(s: &str) -> Option<GitalyEvent> {
        let code_start = s.find("grpc.code=")? + 10;
        let sub_str = s.get(code_start..)?;
        let code_end = sub_str.find(' ')?;
        let code = sub_str.get(..code_end)?;

        let meth_start = sub_str.find("grpc.method=")? + 12;
        let sub_str = sub_str.get(meth_start..)?;
        let meth_end = sub_str.find(' ')?;
        let method = sub_str.get(..meth_end)?;

        let dur_start_time = sub_str.find("grpc.time_ms=")? + 13;
        let sub_str = sub_str.get(dur_start_time..)?;
        let dur_end = sub_str.find(' ')?;
        let dur = sub_str.get(..dur_end)?;

        let status = if code == "OK" {
            Status::Success
        } else {
            Status::Fail
        };

        Some(GitalyEvent {
            method: method.to_owned(),
            time_ms: dur.parse::<f64>().unwrap_or_default(),
            status,
        })
    }

    fn parse_time(s: &str) -> Option<NaiveDateTime> {
        let time_str_end = s.find(' ')?;
        let time_str = s.get(..time_str_end)?;
        NaiveDateTime::parse_from_str(&time_str, "%Y-%m-%d_%H:%M:%S%.f").ok()
    }
}

#[derive(Deserialize)]
pub struct GitalyJsonEvent {
    #[serde(rename = "grpc.method")]
    method: String,
    #[serde(rename = "grpc.time_ms")]
    time_ms: f64,
    #[serde(rename = "grpc.code")]
    code: String,
}

impl Event for GitalyJsonEvent {
    #[inline]
    fn deconstruct(self) -> (String, f64, Status, StatsType) {
        let status = if self.code == "OK" {
            Status::Success
        } else {
            Status::Fail
        };
        (self.method, self.time_ms, status, StatsType::Gitaly)
    }

    #[inline]
    fn parse(s: &str) -> Option<GitalyJsonEvent> {
        serde_json::from_str::<GitalyJsonEvent>(&s).ok()
    }

    fn parse_time(s: &str) -> Option<NaiveDateTime> {
        let untyped: Value = serde_json::from_str(s).ok()?;
        let time_str = untyped.get("time").and_then(|s| s.as_str())?;
        NaiveDateTime::parse_from_str(&time_str[..time_str.len() - 1], "%Y-%m-%dT%H:%M:%S%.f").ok()
    }
}

pub struct SidekiqEvent {
    class: String,
    duration: f64,
    status: Status,
}

impl Event for SidekiqEvent {
    #[inline]
    fn deconstruct(self) -> (String, f64, Status, StatsType) {
        (
            self.class,
            self.duration * 1000.0,
            self.status,
            StatsType::Sidekiq,
        )
    }

    #[inline]
    fn parse(s: &str) -> Option<SidekiqEvent> {
        let sub_str_start = s.find("TID")?;
        let sub_str = s.get(sub_str_start..)?;
        let class_start = sub_str.find(' ')? + 1;
        let sub_str = sub_str.get(class_start..)?;
        let class_end = sub_str.find(' ')?;

        let class = sub_str.get(..class_end)?;

        if let Some(done_start) = sub_str.find("done:") {
            let sub_str = sub_str.get(done_start..)?;
            let dur_start = sub_str.find(' ')? + 1;
            let sub_str = sub_str.get(dur_start..)?;
            let dur_end = sub_str.find(' ')?;
            let dur = sub_str.get(..dur_end)?;

            Some(SidekiqEvent {
                class: class.to_owned(),
                duration: dur.parse::<f64>().unwrap_or_default(),
                status: Status::Success,
            })
        } else if let Some(fail_start) = sub_str.find("fail:") {
            let sub_str = sub_str.get(fail_start..)?;
            let dur_start = sub_str.find(' ')? + 1;
            let sub_str = sub_str.get(dur_start..)?;
            let dur_end = sub_str.find(' ')?;
            let dur = sub_str.get(..dur_end)?;

            Some(SidekiqEvent {
                class: class.to_owned(),
                duration: dur.parse::<f64>().unwrap_or_default(),
                status: Status::Fail,
            })
        } else {
            None
        }
    }

    fn parse_time(s: &str) -> Option<NaiveDateTime> {
        let time_str_end = s.find(' ')?;
        let time_str = s.get(..time_str_end)?;
        NaiveDateTime::parse_from_str(&time_str, "%Y-%m-%d_%H:%M:%S%.f").ok()
    }
}

#[derive(Deserialize)]
pub struct SidekiqJsonEvent {
    class: String,
    duration: f64,
    #[serde(rename = "job_status")]
    status: String,
}

impl Event for SidekiqJsonEvent {
    #[inline]
    fn deconstruct(self) -> (String, f64, Status, StatsType) {
        let status = if self.status == "done" {
            Status::Success
        } else {
            Status::Fail
        };
        (
            self.class,
            self.duration * 1000.0,
            status,
            StatsType::Sidekiq,
        )
    }

    #[inline]
    fn parse(s: &str) -> Option<SidekiqJsonEvent> {
        serde_json::from_str::<SidekiqJsonEvent>(&s).ok()
    }

    fn parse_time(s: &str) -> Option<NaiveDateTime> {
        let untyped: Value = serde_json::from_str(s).ok()?;
        let time_str = untyped.get("time").and_then(|s| s.as_str())?;
        NaiveDateTime::parse_from_str(&time_str[..time_str.len() - 1], "%Y-%m-%dT%H:%M:%S%.f").ok()
    }
}

#[derive(Deserialize)]
pub struct SidekiqJsonDurSEvent {
    class: String,
    duration_s: f64,
    #[serde(rename = "job_status")]
    status: String,
}

impl Event for SidekiqJsonDurSEvent {
    #[inline]
    fn deconstruct(self) -> (String, f64, Status, StatsType) {
        let status = if self.status == "done" {
            Status::Success
        } else {
            Status::Fail
        };
        (
            self.class,
            self.duration_s * 1000.0,
            status,
            StatsType::Sidekiq,
        )
    }

    #[inline]
    fn parse(s: &str) -> Option<SidekiqJsonDurSEvent> {
        serde_json::from_str::<SidekiqJsonDurSEvent>(&s).ok()
    }

    fn parse_time(s: &str) -> Option<NaiveDateTime> {
        let untyped: Value = serde_json::from_str(s).ok()?;
        let time_str = untyped.get("time").and_then(|s| s.as_str())?;
        NaiveDateTime::parse_from_str(&time_str[..time_str.len() - 1], "%Y-%m-%dT%H:%M:%S%.f").ok()
    }
}
