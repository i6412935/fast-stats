use raw_data::RawData;
use stats_vec::{PrintFormat, SortBy};
use std::collections::HashMap;
use std::fmt;
use std::io::ErrorKind;
use std::path::Path;

mod bench;
mod calc;
mod check_type;
mod cli;
mod event;
mod execute;
mod raw_data;
mod stats;
mod stats_vec;

#[global_allocator]
static ALLOC: jemallocator::Jemalloc = jemallocator::Jemalloc;

#[derive(Clone, Copy, Debug, PartialEq)]
pub struct LogType {
    pub format: LogFormat,
    pub app: AppType,
}

impl LogType {
    pub fn new(format: LogFormat, app: AppType) -> LogType {
        LogType { format, app }
    }
}

#[derive(Clone, Copy, Debug, PartialEq)]
pub enum LogFormat {
    ApiJson,
    ApiJsonDurS,
    GitalyText,
    GitalyJson,
    ProdJson,
    ProdJsonDurS,
    SidekiqText,
    SidekiqJson,
    SidekiqJsonDurS,
}

#[derive(Clone, Copy, Debug, PartialEq)]
pub enum AppType {
    RailsApi,
    Gitaly,
    RailsProd,
    Sidekiq,
}

impl fmt::Display for AppType {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        use AppType::*;
        let desc = match self {
            RailsApi => "api_json",
            Gitaly => "gitaly",
            RailsProd => "production_json",
            Sidekiq => "sidekiq",
        };

        write!(f, "{}", desc)
    }
}

#[derive(Clone, Copy, Debug)]
pub enum Input<'a> {
    File(&'a str),
    Stdin,
}

impl<'a> Input<'a> {
    fn file_name(&self) -> String {
        match &self {
            Input::File(file) => String::from(
                // Remove leading path if present
                Path::new(file)
                    .file_name()
                    .unwrap_or_default()
                    .to_string_lossy(),
            ),
            Input::Stdin => String::from("stdin"),
        }
    }
}

#[derive(Clone, Debug)]
pub struct Args<'a> {
    input: Input<'a>,
    sort_by: SortBy,
    bench: Option<&'a str>,
    print_format: PrintFormat,
    requested_log_type: Option<LogType>,
    thread_ct: usize,
    limit: Option<usize>,
    force_color: bool,
}

fn main() {
    let cli_args = cli::cli_args().get_matches();

    let input = match cli_args.value_of("file") {
        Some(f) => Input::File(f),
        None => Input::Stdin,
    };

    let sort_by = if let Some(sort) = cli_args.value_of("sort_by") {
        use SortBy::*;
        match sort {
            "count" => Count,
            "fail" => Fail,
            "max" => Max,
            "median" => Median,
            "min" => Min,
            "perc99" => P99,
            "perc95" => P95,
            "rps" => Rps,
            "score" => Score,
            _ => unreachable!(),
        }
    } else {
        SortBy::Score
    };

    let bench = cli_args.value_of("benchmark");

    let print_format = if let Some(format) = cli_args.value_of("format") {
        use PrintFormat::*;
        match (format, cli_args.is_present("mini")) {
            ("csv", false) => FullCsv,
            ("md", false) => FullMd,
            (_, false) => FullText,
            ("csv", true) => MiniCsv,
            ("md", true) => MiniMd,
            (_, true) => MiniText,
        }
    } else if cli_args.is_present("mini") {
        PrintFormat::MiniText
    } else {
        PrintFormat::FullText
    };

    let requested_log_type = if let Some(log_type) = cli_args.value_of("log_type") {
        use AppType::*;
        use LogFormat::*;
        match log_type {
            "api_json" => Some(LogType::new(ApiJson, RailsApi)),
            "api_json_dur_s" => Some(LogType::new(ApiJsonDurS, RailsApi)),
            "gitaly" => Some(LogType::new(GitalyText, Gitaly)),
            "gitaly_json" => Some(LogType::new(GitalyJson, Gitaly)),
            "production_json" => Some(LogType::new(ProdJson, RailsProd)),
            "production_json_dur_s" => Some(LogType::new(ProdJsonDurS, RailsProd)),
            "sidekiq" => Some(LogType::new(SidekiqText, Sidekiq)),
            "sidekiq_json" => Some(LogType::new(SidekiqJson, Sidekiq)),
            "sidekiq_json_dur_s" => Some(LogType::new(SidekiqJsonDurS, Sidekiq)),
            _ => unreachable!(),
        }
    } else {
        None
    };

    let compare_file = match cli_args.value_of("compare") {
        Some(comp) => Some(Input::File(comp)),
        None => None,
    };

    let limit = match cli_args.value_of("limit") {
        Some(comp) => Some(comp.parse::<usize>().expect("Invalid limit")),
        None => None,
    };

    let force_color = cli_args.is_present("force_color");

    let parsed_args = Args {
        input,
        sort_by,
        bench,
        print_format,
        requested_log_type,
        thread_ct: num_cpus::get_physical(),
        limit,
        force_color,
    };

    let result = match (compare_file, bench) {
        (Some(comp), _) => execute::exec_compare(parsed_args, comp),
        (_, Some(bench_vers)) => execute::exec_bench(parsed_args, String::from(bench_vers)),
        (None, None) => execute::exec_stats(parsed_args),
    };

    if let Err(e) = result {
        match e.kind() {
            ErrorKind::BrokenPipe => {} //ignore errors caused by piping
            _ => {
                eprintln!("Error - {}", e);
                std::process::exit(1);
            }
        }
    }
}
