use crate::stats::StatsType;

#[derive(Debug)]
pub struct RawData {
    pub data: Vec<f64>,
    pub fails: usize,
    pub stats_type: StatsType,
}

impl RawData {
    #[inline]
    pub fn new(stats_type: StatsType) -> RawData {
        RawData {
            data: Vec::new(),
            fails: 0,
            stats_type,
        }
    }
}
