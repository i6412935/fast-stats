use crate::raw_data::RawData;
use crate::stats::{PrimField, Stats, StatsType};
use crate::HashMap;
use atty::Stream;
use chrono::Duration;
use rayon::prelude::*;
use std::io::{stdout, Error, Write};

#[derive(Clone, Copy, Debug)]
pub enum SortBy {
    Score,
    Count,
    Fail,
    Max,
    Median,
    Min,
    P99,
    P95,
    Rps,
}

#[derive(Clone, Copy, Debug)]
pub enum PrintFormat {
    FullText,
    FullMd,
    FullCsv,
    MiniText,
    MiniMd,
    MiniCsv,
}

#[derive(Clone, Debug)]
pub struct StatsVec(Vec<Stats>);

impl StatsVec {
    pub fn new(stats: Vec<Stats>) -> StatsVec {
        StatsVec(stats)
    }

    pub fn sort_by(&mut self, sort_by: SortBy) {
        match sort_by {
            SortBy::Count => {
                self.0.par_sort_unstable_by(|x, y| (&y.count).cmp(&x.count));
            }
            SortBy::Rps => {
                self.0.par_sort_unstable_by(|x, y| {
                    (&y.rps)
                        .partial_cmp(&x.rps)
                        .expect("Invalid comparison when sorting")
                });
            }
            SortBy::Fail => {
                self.0.par_sort_unstable_by(|x, y| {
                    (&y.perc_failed)
                        .partial_cmp(&x.perc_failed)
                        .expect("Invalid comparison when sorting")
                });
            }
            SortBy::Max => {
                self.0.par_sort_unstable_by(|x, y| {
                    (&y.max)
                        .partial_cmp(&x.max)
                        .expect("Invalid comparison when sorting")
                });
            }
            SortBy::Median => {
                self.0.par_sort_unstable_by(|x, y| {
                    (&y.median)
                        .partial_cmp(&x.median)
                        .expect("Invalid comparison when sorting")
                });
            }
            SortBy::Min => {
                self.0.par_sort_unstable_by(|x, y| {
                    (&y.min)
                        .partial_cmp(&x.min)
                        .expect("Invalid comparison when sorting")
                });
            }
            SortBy::P99 => {
                self.0.par_sort_unstable_by(|x, y| {
                    (&y.p99)
                        .partial_cmp(&x.p99)
                        .expect("Invalid comparison when sorting")
                });
            }
            SortBy::P95 => {
                self.0.par_sort_unstable_by(|x, y| {
                    (&y.p95)
                        .partial_cmp(&x.p95)
                        .expect("Invalid comparison when sorting")
                });
            }
            SortBy::Score => {
                self.0.par_sort_unstable_by(|x, y| {
                    (&y.score)
                        .partial_cmp(&x.score)
                        .expect("Invalid comparison when sorting")
                });
            }
        }
    }

    pub fn print_csv(&self, limit: Option<usize>) -> Result<(), Error> {
        if let Some(s) = self.0.first() {
            s.print_csv_header()?;
        }

        let limit = match limit {
            Some(l) => l,
            None => self.0.len(),
        };

        for stat in self.0.iter().take(limit) {
            stat.print_csv()?;
        }

        Ok(())
    }

    pub fn print_mini_csv(&self, limit: Option<usize>) -> Result<(), Error> {
        if let Some(s) = &self.0.first() {
            s.print_mini_csv_header()?;
        }

        let limit = match limit {
            Some(l) => l,
            None => self.0.len(),
        };

        for stat in self.0.iter().take(limit) {
            stat.print_mini_csv()?;
        }

        Ok(())
    }

    pub fn print_md(&self, limit: Option<usize>) -> Result<(), Error> {
        if let Some(s) = self.0.first() {
            s.print_md_header()?;
        }

        let limit = match limit {
            Some(l) => l,
            None => self.0.len(),
        };

        for stat in self.0.iter().take(limit) {
            stat.print_md()?;
        }

        Ok(())
    }

    pub fn print_mini_md(&self, limit: Option<usize>) -> Result<(), Error> {
        if let Some(s) = &self.0.first() {
            s.print_mini_md_header()?;
        }

        let limit = match limit {
            Some(l) => l,
            None => self.0.len(),
        };

        for stat in self.0.iter().take(limit) {
            stat.print_mini_md()?;
        }

        Ok(())
    }

    pub fn print_text(&self, limit: Option<usize>) -> Result<(), Error> {
        let lens = self.column_lens();

        if let Some(s) = self.0.first() {
            s.print_text_header(&lens)?;
        }

        let limit = match limit {
            Some(l) => l,
            None => self.0.len(),
        };

        for stat in self.0.iter().take(limit) {
            stat.print_text(PrimField::Print, &lens)?;
        }

        Ok(())
    }

    pub fn print_mini_text(&self, limit: Option<usize>) -> Result<(), Error> {
        let lens = self.column_lens();

        if let Some(s) = self.0.first() {
            s.print_mini_text_header(&lens)?;
        }

        let limit = match limit {
            Some(l) => l,
            None => self.0.len(),
        };

        for stat in self.0.iter().take(limit) {
            stat.print_mini_text(PrimField::Print, &lens)?;
        }

        Ok(())
    }

    pub fn column_lens(&self) -> [usize; 10] {
        [
            self.primary_column_len(),
            self.find_column_len(|s| s.count as usize, 8),
            self.find_column_len(|s| s.rps as usize, 3) + 3,
            self.find_column_len(|s| s.p99 as usize, 5) + 3,
            self.find_column_len(|s| s.p95 as usize, 5) + 3,
            self.find_column_len(|s| s.median as usize, 5) + 3,
            self.find_column_len(|s| s.max as usize, 5) + 3,
            self.find_column_len(|s| s.min as usize, 5) + 3,
            self.find_column_len(|s| s.score as usize, 5) + 3,
            self.find_column_len(|s| s.perc_failed as usize, 5) + 3,
        ]
    }

    pub fn stats_type(&self) -> Option<StatsType> {
        self.0.get(0).map(|s| s.stats_type.clone())
    }

    fn find_column_len<F>(&self, field_fn: F, min_len: usize) -> usize
    where
        F: FnMut(&Stats) -> usize,
    {
        let max = self.0.iter().map(field_fn).max().unwrap_or_default();
        (format!("{}", max).len()).max(min_len)
    }

    fn primary_column_len(&self) -> usize {
        self.0
            .iter()
            .map(|s| s.prim_field.len())
            .max()
            .unwrap_or(15)
    }
}

impl From<(HashMap<String, RawData>, Duration)> for StatsVec {
    fn from(data: (HashMap<String, RawData>, Duration)) -> Self {
        let (mut event_map, dur) = data;
        let stats: Vec<_> = event_map
            .drain()
            .map(|(k, v)| Stats::from((k, v, dur)))
            .collect();

        StatsVec(stats)
    }
}

#[derive(Clone, Debug)]
pub struct StatsDiff<'a> {
    pub stats: &'a Stats,
    pub other_stats: &'a Stats,
    file_name: &'a str,
    other_file_name: &'a str,
    ct_diff: f64,
    rps_diff: f64,
    p99_diff: f64,
    p95_diff: f64,
    med_diff: f64,
    max_diff: f64,
    min_diff: f64,
    scr_diff: f64,
    fail_diff: f64,
}

impl<'a> StatsDiff<'a> {
    fn new(
        stat: &'a Stats,
        other_stats: &'a Stats,
        file_name: &'a str,
        other_file_name: &'a str,
    ) -> StatsDiff<'a> {
        let ct_diff = StatsDiff::calc_ratio(stat.count as f64, other_stats.count as f64);
        let rps_diff = StatsDiff::calc_ratio(stat.rps, other_stats.rps);
        let p99_diff = StatsDiff::calc_ratio(stat.p99, other_stats.p99);
        let p95_diff = StatsDiff::calc_ratio(stat.p95, other_stats.p95);
        let med_diff = StatsDiff::calc_ratio(stat.median, other_stats.median);
        let max_diff = StatsDiff::calc_ratio(stat.max, other_stats.max);
        let min_diff = StatsDiff::calc_ratio(stat.min, other_stats.min);
        let scr_diff = StatsDiff::calc_ratio(stat.score, other_stats.score);
        let fail_diff = StatsDiff::calc_ratio(stat.perc_failed, other_stats.perc_failed);

        StatsDiff {
            stats: stat,
            other_stats,
            file_name,
            other_file_name,
            ct_diff,
            rps_diff,
            p99_diff,
            p95_diff,
            med_diff,
            max_diff,
            min_diff,
            scr_diff,
            fail_diff,
        }
    }

    fn calc_ratio(stat: f64, other: f64) -> f64 {
        let ratio = other / stat;

        if ratio.is_nan() {
            0.0
        } else {
            ratio
        }
    }

    pub fn print_csv_header(&self) -> Result<(), Error> {
        writeln!(
            stdout(),
            "FILE,{},COUNT,RPS,PERC99,PERC95,MEDIAN,MAX,MIN,SCORE,%_FAIL",
            self.stats.stats_type.header_title(),
        )?;

        Ok(())
    }

    pub fn print_csv_diffs(&self) -> Result<(), Error> {
        write!(stdout(), "RATIO")?;
        writeln!(
            stdout(),
            ",{},{:.2}x,{:.2}x,{:.2}x,{:.2}x,{:.2}x,{:.2}x,{:.2}x,{:.2}x,{:.2}x",
            self.stats.prim_field,
            self.ct_diff,
            self.rps_diff,
            self.p99_diff,
            self.p95_diff,
            self.med_diff,
            self.max_diff,
            self.min_diff,
            self.scr_diff,
            self.fail_diff,
        )?;

        Ok(())
    }

    pub fn print_mini_csv_header(&self) -> Result<(), Error> {
        writeln!(
            stdout(),
            "FILE,{},COUNT,RPS,PERC99,%_FAIL",
            self.stats.stats_type.header_title(),
        )?;

        Ok(())
    }

    pub fn print_mini_csv_diffs(&self) -> Result<(), Error> {
        write!(stdout(), "RATIO")?;

        writeln!(
            stdout(),
            ",{},{:.2}x,{:.2}x,{:.2}x,{:.2}x",
            self.stats.prim_field,
            self.ct_diff,
            self.rps_diff,
            self.p99_diff,
            self.fail_diff,
        )?;

        Ok(())
    }

    pub fn print_md_header(&self) -> Result<(), Error> {
        writeln!(
            stdout(),
            "|FILE|{}|COUNT|RPS|PERC99|PERC95|MEDIAN|MAX|MIN|SCORE|FAIL|\n\
             |--|--|--|--|--|--|--|--|--|--|--|",
            self.stats.stats_type.header_title(),
        )?;

        Ok(())
    }

    pub fn print_md_diffs(&self) -> Result<(), Error> {
        writeln!(
            stdout(),
            "|{}|{}|{:.2}x|{:.2}x|{:.2}x|{:.2}x|{:.2}x|{:.2}x|{:.2}x|{:.2}x|{:.2}x|",
            "RATIO",
            "",
            self.ct_diff,
            self.rps_diff,
            self.p99_diff,
            self.p95_diff,
            self.med_diff,
            self.max_diff,
            self.min_diff,
            self.scr_diff,
            self.fail_diff,
        )?;

        Ok(())
    }

    pub fn print_mini_md_header(&self) -> Result<(), Error> {
        writeln!(
            stdout(),
            "|FILE|{}|COUNT|RPS|PERC99|% FAIL|\n\
             |--|--|--|--|--|--|",
            self.stats.stats_type.header_title(),
        )?;

        Ok(())
    }

    pub fn print_mini_md_diffs(&self) -> Result<(), Error> {
        writeln!(
            stdout(),
            "|{}|{}|{:.2}x|{:.2}x|{:.2}x|{:.2}x|",
            "RATIO",
            "",
            self.ct_diff,
            self.rps_diff,
            self.p99_diff,
            self.fail_diff,
        )?;

        Ok(())
    }

    fn print_text_diffs(&self, lens: &[usize], force_color: bool) -> Result<(), Error> {
        if atty::is(Stream::Stdout) || force_color {
            writeln!(
                stdout(),
                "{: <prim$} {} {} {} {} {} {} {} {} {}",
                "",
                format!("{: >ct$.2}x", self.ct_diff, ct = lens[1]),
                format!("{: >rps$.2}x", self.rps_diff, rps = lens[2]),
                colorize(self.p99_diff, lens[3]),
                colorize(self.p95_diff, lens[4]),
                colorize(self.med_diff, lens[5]),
                colorize(self.max_diff, lens[6]),
                colorize(self.min_diff, lens[7]),
                format!("{: >scr$.2}x", self.scr_diff, scr = lens[8]),
                colorize(self.fail_diff, lens[9]),
                prim = lens[0],
            )?;
        } else {
            writeln!(
                stdout(),
                "{: <prim$} {: >ct$.2}x {: >rps$.2}x {: >p99$.2}x {: >p95$.2}x {: >med$.2}x \
                 {: >max$.2}x {: >min$.2}x {: >scr$.2}x {: >fld$.2}x",
                "",
                self.ct_diff,
                self.rps_diff,
                self.p99_diff,
                self.p95_diff,
                self.med_diff,
                self.max_diff,
                self.min_diff,
                self.scr_diff,
                self.fail_diff,
                prim = lens[0],
                ct = lens[1],
                rps = lens[2],
                p99 = lens[3],
                p95 = lens[4],
                med = lens[5],
                max = lens[6],
                min = lens[7],
                scr = lens[8],
                fld = lens[9],
            )?;
        }

        Ok(())
    }

    fn print_mini_text_diffs(&self, lens: &[usize], force_color: bool) -> Result<(), Error> {
        if atty::is(Stream::Stdout) || force_color {
            writeln!(
                stdout(),
                "{: <prim$} {} {} {} {}",
                "",
                format!("{: >ct$.2}x", self.ct_diff, ct = lens[1]),
                format!("{: >rps$.2}x", self.rps_diff, rps = lens[2]),
                colorize(self.p99_diff, lens[3]),
                colorize(self.fail_diff, lens[9]),
                prim = lens[0],
            )?;
        } else {
            writeln!(
                stdout(),
                "{: <prim$} {: >ct$.2}x {: >rps$.2}x {: >p99$.2}x {: >fld$.2}x",
                "",
                self.ct_diff,
                self.rps_diff,
                self.p99_diff,
                self.fail_diff,
                prim = lens[0],
                ct = lens[1],
                rps = lens[2],
                p99 = lens[3],
                fld = lens[9],
            )?;
        }

        Ok(())
    }
}

#[derive(Clone, Debug)]
pub struct StatsDiffVec<'a> {
    diffs: Vec<StatsDiff<'a>>,
    max_lens: Vec<usize>,
}

impl<'a> StatsDiffVec<'a> {
    pub fn new(
        stats_vec: &'a StatsVec,
        other_stats_vec: &'a StatsVec,
        file_name: &'a str,
        other_file_name: &'a str,
    ) -> StatsDiffVec<'a> {
        let mut diffs = Vec::new();

        for other_stat in &other_stats_vec.0 {
            if let Some(stat) = stats_vec
                .0
                .iter()
                .find(|s| s.prim_field == other_stat.prim_field)
            {
                let diff = StatsDiff::new(stat, other_stat, file_name, other_file_name);
                diffs.push(diff);
            }
        }

        let first_lens = stats_vec.column_lens();
        let other_lens = other_stats_vec.column_lens();

        let max_lens: Vec<_> = first_lens
            .iter()
            .zip(&other_lens)
            .map(|(&x, &y)| if x > y { x } else { y })
            .collect();

        StatsDiffVec { diffs, max_lens }
    }

    pub fn print_csv(&self, limit: Option<usize>) -> Result<(), Error> {
        if let Some(first_diff) = &self.diffs.first() {
            first_diff.print_csv_header()?;
        }

        let limit = match limit {
            Some(l) => l,
            None => self.len(),
        };

        for diff in self.diffs.iter().take(limit) {
            write!(stdout(), "{},", diff.file_name)?;
            diff.stats.print_csv()?;
        }

        for diff in self.diffs.iter().take(limit) {
            write!(stdout(), "{},", diff.other_file_name)?;
            diff.other_stats.print_csv()?;
        }

        for diff in self.diffs.iter().take(limit) {
            diff.print_csv_diffs()?;
        }

        Ok(())
    }

    pub fn print_mini_csv(&self, limit: Option<usize>) -> Result<(), Error> {
        if let Some(first_diff) = &self.diffs.first() {
            first_diff.print_mini_csv_header()?;
        }

        let limit = match limit {
            Some(l) => l,
            None => self.len(),
        };

        for diff in self.diffs.iter().take(limit) {
            write!(stdout(), "{},", diff.file_name)?;
            diff.stats.print_mini_csv()?;
        }

        for diff in self.diffs.iter().take(limit) {
            write!(stdout(), "{},", diff.other_file_name)?;
            diff.other_stats.print_mini_csv()?;
        }

        for diff in self.diffs.iter().take(limit) {
            diff.print_mini_csv_diffs()?;
        }

        Ok(())
    }

    pub fn print_md(&self, limit: Option<usize>) -> Result<(), Error> {
        if let Some(first_diff) = &self.diffs.first() {
            first_diff.print_md_header()?;
        }

        let limit = match limit {
            Some(l) => l,
            None => self.len(),
        };

        for diff in self.diffs.iter().take(limit) {
            write!(stdout(), "|{}", diff.file_name)?;
            diff.stats.print_md()?;

            write!(stdout(), "|{}", diff.other_file_name)?;
            diff.other_stats.print_md()?;

            diff.print_md_diffs()?;
        }

        Ok(())
    }

    pub fn print_mini_md(&self, limit: Option<usize>) -> Result<(), Error> {
        if let Some(first_diff) = &self.diffs.first() {
            first_diff.print_mini_md_header()?;
        }

        let limit = match limit {
            Some(l) => l,
            None => self.len(),
        };

        for diff in self.diffs.iter().take(limit) {
            write!(stdout(), "|{}", diff.file_name)?;
            diff.stats.print_mini_md()?;

            write!(stdout(), "|{}", diff.other_file_name)?;
            diff.other_stats.print_mini_md()?;

            diff.print_mini_md_diffs()?;
        }

        Ok(())
    }

    pub fn print_text(&self, limit: Option<usize>, force_color: bool) -> Result<(), Error> {
        if let Some(first_diff) = &self.diffs.first() {
            let file_len = first_diff
                .file_name
                .len()
                .max(first_diff.other_file_name.len());

            write!(stdout(), "{:<fn_len$}  ", "FILE", fn_len = file_len)?;
            first_diff.stats.print_text_header(&self.max_lens)?;

            let limit = match limit {
                Some(l) => l,
                None => self.len(),
            };

            for diff in self.diffs.iter().take(limit) {
                write!(
                    stdout(),
                    "{:<fn_len$}  ",
                    first_diff.file_name,
                    fn_len = file_len
                )?;
                diff.stats.print_text(PrimField::Print, &self.max_lens)?;

                write!(
                    stdout(),
                    "{:<fn_len$}  ",
                    first_diff.other_file_name,
                    fn_len = file_len
                )?;
                diff.other_stats
                    .print_text(PrimField::Blank, &self.max_lens)?;

                write!(stdout(), "{:<fn_len$}  ", "ratio", fn_len = file_len)?;
                diff.print_text_diffs(&self.max_lens, force_color)?;

                writeln!(stdout())?;
            }
        }

        Ok(())
    }

    pub fn print_mini_text(&self, limit: Option<usize>, force_color: bool) -> Result<(), Error> {
        if let Some(first_diff) = &self.diffs.first() {
            let file_len = first_diff
                .file_name
                .len()
                .max(first_diff.other_file_name.len());

            write!(stdout(), "{:<fn_len$}  ", "FILE", fn_len = file_len)?;
            first_diff.stats.print_mini_text_header(&self.max_lens)?;

            let limit = match limit {
                Some(l) => l,
                None => self.len(),
            };

            for diff in self.diffs.iter().take(limit) {
                write!(
                    stdout(),
                    "{:<fn_len$}  ",
                    first_diff.file_name,
                    fn_len = file_len
                )?;
                diff.stats
                    .print_mini_text(PrimField::Print, &self.max_lens)?;

                write!(
                    stdout(),
                    "{:<fn_len$}  ",
                    first_diff.other_file_name,
                    fn_len = file_len
                )?;
                diff.other_stats
                    .print_mini_text(PrimField::Blank, &self.max_lens)?;

                write!(stdout(), "{:<fn_len$}  ", "ratio", fn_len = file_len)?;
                diff.print_mini_text_diffs(&self.max_lens, force_color)?;

                writeln!(stdout())?;
            }
        }

        Ok(())
    }

    fn len(&self) -> usize {
        self.diffs.len()
    }
}

fn colorize(num: f64, len: usize) -> String {
    if num > 5.0 {
        format!("{}{: >l$.2}x{}", RED, num, RESET, l = len)
    } else if num >= 2.0 {
        format!("{}{: >l$.2}x{}", YELLOW, num, RESET, l = len)
    } else if num <= 0.66 && num > 0.0 {
        format!("{}{: >l$.2}x{}", BRIGHT_GREEN, num, RESET, l = len)
    } else if num < 0.25 && num > 0.0 {
        format!("{}{: >l$.2}x{}", GREEN, num, RESET, l = len)
    } else {
        format!("{: >l$.2}x", num, l = len)
    }
}

const BRIGHT_GREEN: &str = "\x1b[32;1m";
const GREEN: &str = "\x1b[32m";
const RED: &str = "\x1b[31;1m";
const RESET: &str = "\x1b[0m";
const YELLOW: &str = "\x1b[33m";
