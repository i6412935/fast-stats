use crate::RawData;
use chrono::Duration;
use rayon::prelude::*;
use std::io::{stdout, Error, Write};

#[derive(Clone, Copy, Debug)]
pub enum StatsType {
    Api,
    Gitaly,
    Prod,
    Sidekiq,
}

#[derive(Clone, Copy, Debug)]
pub enum PrimField {
    Blank,
    Print,
}

impl StatsType {
    pub fn header_title(self) -> &'static str {
        use StatsType::*;
        match &self {
            Api => "ROUTE",
            Gitaly => "METHOD",
            Prod => "CONTROLLER",
            Sidekiq => "WORKER",
        }
    }
}

#[derive(Clone, Debug)]
pub struct Stats {
    pub stats_type: StatsType,
    pub prim_field: String,
    pub count: usize,
    pub rps: f64,
    pub p99: f64,
    pub p95: f64,
    pub median: f64,
    pub max: f64,
    pub min: f64,
    pub score: f64,
    pub perc_failed: f64,
}

impl From<(String, RawData, Duration)> for Stats {
    fn from(data: (String, RawData, Duration)) -> Self {
        let (prim_field, mut raw_data, dur) = data;
        raw_data.data.par_sort_unstable_by(|x, y| {
            (x).partial_cmp(y)
                .expect("Invalid comparison when sorting durations")
        });

        let count = raw_data.data.len();
        let rps = count as f64 / dur.num_seconds() as f64;
        let p99 = Self::percentile(&raw_data.data, 99);
        let p95 = Self::percentile(&raw_data.data, 95);
        let median = Self::percentile(&raw_data.data, 50);
        let max = *raw_data.data.last().unwrap_or(&0.0);
        let min = *raw_data.data.first().unwrap_or(&0.0);
        let score = p99 * count as f64;
        let perc_failed = (raw_data.fails as f64 / count as f64) * 100.0;

        Stats {
            stats_type: raw_data.stats_type,
            prim_field,
            count,
            rps,
            median,
            p99,
            p95,
            max,
            min,
            score,
            perc_failed,
        }
    }
}

impl Stats {
    pub fn print_csv_header(&self) -> Result<(), Error> {
        writeln!(
            stdout(),
            "{},COUNT,RPS,PERC99,PERC95,MEDIAN,MAX,MIN,SCORE,%_FAIL",
            self.stats_type.header_title(),
        )?;

        Ok(())
    }

    pub fn print_csv(&self) -> Result<(), Error> {
        writeln!(
            stdout(),
            "{},{},{:.2},{:.2},{:.2},{:.2},{:.2},{:.2},{:.2},{:.2}%",
            self.prim_field,
            self.count,
            self.rps,
            self.p99,
            self.p95,
            self.median,
            self.max,
            self.min,
            self.score,
            self.perc_failed,
        )?;

        Ok(())
    }

    pub fn print_mini_csv_header(&self) -> Result<(), Error> {
        writeln!(
            stdout(),
            "{},COUNT,RPS,PERC99,%_FAIL",
            self.stats_type.header_title(),
        )?;

        Ok(())
    }

    pub fn print_mini_csv(&self) -> Result<(), Error> {
        writeln!(
            stdout(),
            "{},{},{:.2},{:.2},{:.2}%",
            self.prim_field,
            self.count,
            self.rps,
            self.p99,
            self.perc_failed
        )?;

        Ok(())
    }

    pub fn print_md_header(&self) -> Result<(), Error> {
        writeln!(
            stdout(),
            "|{}|COUNT|RPS|PERC99|PERC95|MEDIAN|MAX|MIN|SCORE|% FAIL|\n|--|--|--|--|--|--|--|--|--|--|",
            self.stats_type.header_title(),
        )?;

        Ok(())
    }

    pub fn print_md(&self) -> Result<(), Error> {
        writeln!(
            stdout(),
            "|{}|{}|{:.2}|{:.2}|{:.2}|{:.2}|{:.2}|{:.2}|{:.2}|{:.2}%|",
            self.prim_field,
            self.count,
            self.rps,
            self.p99,
            self.p95,
            self.median,
            self.max,
            self.min,
            self.score,
            self.perc_failed,
        )?;

        Ok(())
    }

    pub fn print_mini_md_header(&self) -> Result<(), Error> {
        writeln!(
            stdout(),
            "|{}|COUNT|RPS|PERC99|% FAIL|\n|--|--|--|--|--|",
            self.stats_type.header_title(),
        )?;

        Ok(())
    }

    pub fn print_mini_md(&self) -> Result<(), Error> {
        writeln!(
            stdout(),
            "|{}|{}|{:.2}|{:.2}|{:.2}%|",
            self.prim_field,
            self.count,
            self.rps,
            self.p99,
            self.perc_failed,
        )?;

        Ok(())
    }

    pub fn print_text_header(&self, lens: &[usize]) -> Result<(), Error> {
        writeln!(
            stdout(),
            "{: <prim$} {: >ct$}  {: >rps$}  {: >p99$}  {: >p95$}  {: >med$}  {: >max$}  {: >min$}  {: >scr$}  {: >fld$}",
            self.stats_type.header_title(),
            "COUNT",
            "RPS",
            "PERC99",
            "PERC95",
            "MEDIAN",
            "MAX",
            "MIN",
            "SCORE",
            "% FAIL",
            prim = lens[0],
            ct = lens[1],
            rps = lens[2],
            p99 = lens[3],
            p95 = lens[4],
            med = lens[5],
            max = lens[6],
            min = lens[7],
            scr = lens[8],
            fld = lens[9],
        )?;

        Ok(())
    }

    pub fn print_text(&self, print_prim: PrimField, lens: &[usize]) -> Result<(), Error> {
        let prim = match print_prim {
            PrimField::Blank => "",
            PrimField::Print => &self.prim_field,
        };

        writeln!(
            stdout(),
            "{: <prim$} {: >ct$}  {: >rps$.2}  {: >p99$.2}  {: >p95$.2}  {: >med$.2}  {: >max$.2}  {: >min$.2}  {: >scr$.2}  {: >fld$.2}",
            prim,
            self.count,
            self.rps,
            self.p99,
            self.p95,
            self.median,
            self.max,
            self.min,
            self.score,
            self.perc_failed,
            prim = lens[0],
            ct = lens[1],
            rps = lens[2],
            p99 = lens[3],
            p95 = lens[4],
            med = lens[5],
            max = lens[6],
            min = lens[7],
            scr = lens[8],
            fld = lens[9],
        )?;

        Ok(())
    }

    pub fn print_mini_text_header(&self, lens: &[usize]) -> Result<(), Error> {
        writeln!(
            stdout(),
            "{: <prim$} {: >ct$}  {: >rps$}  {: >p99$}  {: >fld$}",
            self.stats_type.header_title(),
            "COUNT",
            "RPS",
            "PERC99",
            "% FAIL",
            prim = lens[0],
            ct = lens[1],
            rps = lens[2],
            p99 = lens[3],
            fld = lens[9],
        )?;

        Ok(())
    }

    pub fn print_mini_text(&self, print_prim: PrimField, lens: &[usize]) -> Result<(), Error> {
        let prim = match print_prim {
            PrimField::Blank => "",
            PrimField::Print => &self.prim_field,
        };

        writeln!(
            stdout(),
            "{: <prim$} {: >ct$}  {: >rps$.2}  {: >p99$.2}  {: >fld$.2}",
            prim,
            self.count,
            self.rps,
            self.p99,
            self.perc_failed,
            prim = lens[0],
            ct = lens[1],
            rps = lens[2],
            p99 = lens[3],
            fld = lens[9],
        )?;

        Ok(())
    }

    fn percentile(times: &[f64], perc: u32) -> f64 {
        if times.is_empty() {
            return 0.0;
        } else if times.len() == 1 {
            return *times.first().unwrap_or(&0.0);
        } else if perc == 100 {
            return *times.last().unwrap_or(&0.0);
        }

        let idx = f64::from(perc) / 100.0 * (times.len() - 1) as f64;

        let lower = times.get(idx.floor() as usize).unwrap_or(&0.0);
        let upper = times.get(idx.floor() as usize + 1).unwrap_or(&0.0);

        lower + (upper - lower) * (idx - idx.floor())
    }
}
