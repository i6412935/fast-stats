use csv::ReaderBuilder;
use std::env;
use std::fs::File;
use std::io::Write;
use std::path::Path;

fn main() {
    let out_dir = env::var("OUT_DIR").unwrap();

    let codegen_path = Path::new(&out_dir).join("bench_codegen.rs");
    let mut f = File::create(&codegen_path).unwrap();

    let fn_path = Path::new(&out_dir).join("bench_fn.rs");
    let mut fn_f = File::create(&fn_path).unwrap();
    write!(fn_f, "match (bench_vers.as_ref(), stats_type) {{\n").unwrap();

    let bench_dir = Path::new("benchmark-data");
    for entry in bench_dir.read_dir().unwrap() {
        if let Ok(entry) = entry {
            let path = entry.path();
            let mut rdr = ReaderBuilder::new().from_path(&path).unwrap();
            let fn_name = path.file_stem().unwrap().to_string_lossy().to_owned();

            write!(fn_f, "{}", fn_info(&fn_name)).unwrap();
            write!(
                f,
                "pub fn v{}() -> StatsVec {{ \nStatsVec::new(vec![",
                fn_name
            )
            .unwrap();

            for result in rdr.records() {
                let record = result.unwrap();
                write!(
                    f,
                    "Stats {{ stats_type: {}, \
                     prim_field: String::from(\"{}\"), \
                     count: {}, rps: {}, p99: {}, p95: {}, \
                     median: {}, max: {}, min: {}, score: {}, \
                     perc_failed: {} }}, ",
                    header(&fn_name),
                    record[0].to_owned(),
                    record[1].to_owned(),
                    record[2].to_owned(),
                    record[3].to_owned(),
                    record[4].to_owned(),
                    record[5].to_owned(),
                    record[6].to_owned(),
                    record[7].to_owned(),
                    record[8].to_owned(),
                    record[9].to_owned().trim_end_matches('%'),
                )
                .unwrap();
            }
            write!(f, "])}}\n\n").unwrap();
        }
    }
    write!(
        fn_f,
        "_ => {{
            eprintln!(\"Benchmarking data not available for this version\");
            return Ok(());
        }}\n}}"
    )
    .unwrap();
}

fn header(file_name: &str) -> &'static str {
    let log_type = file_name.rsplit('_').next().unwrap();
    match log_type {
        "api" => "StatsType::Api",
        "gitaly" => "StatsType::Gitaly",
        "prod" => "StatsType::Prod",
        "sidekiq" => "StatsType::Sidekiq",
        _ => unreachable!(),
    }
}

fn fn_info(file_name: &str) -> String {
    let mut split = file_name.split('_');

    let major = split.next().unwrap();
    let minor = split.next().unwrap();

    let log_type = split.next().unwrap().to_owned();
    let mut upper_type = log_type.clone();
    upper_type.get_mut(0..1).unwrap().make_ascii_uppercase();

    format!(
        "(\"{}.{}\", {}) => crate::bench::v{}_{}_{}(),\n",
        major, minor, upper_type, major, minor, log_type,
    )
}
